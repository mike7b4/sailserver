# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-sailserver
QT+=network dbus
CONFIG += sailfishapp
CONFIG += sailfishapp_i18n

other.files = ChangeLog.txt LICENSE.txt
other.path = /usr/share/harbour-sailserver/
stylesheet.files = css/stylesheet.css
stylesheet.path = /usr/share/harbour-sailserver/css
html.files = html/latest.html
html.path = /usr/share/harbour-sailserver/html
INSTALLS += other stylesheet html
TRANSLATIONS += translations/harbour-sailserver-sv.ts
TRANSLATIONS += translations/harbour-sailserver-de.ts
TRANSLATIONS += translations/harbour-sailserver-fi.ts
DEFINES+=VERSION=\\\"$$VERSION\\\"
SOURCES += \
    src/appinfo.cpp \
    src/sailhttpserver.cpp \
    src/sailserver.cpp \
    src/sailclientthread.cpp \
    src/permissions.cpp \
    src/httpbytearray.cpp \
    src/controllerhome.cpp \
    src/controllerupload.cpp \
    src/rpminstall.cpp \
    src/controllerdownload.cpp \
    src/controllerbase.cpp \
    src/controllerclipboard.cpp \
    src/settings.cpp \
    src/controllerlatest.cpp

OTHER_FILES += \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/harbour-sailserver.spec \
    rpm/harbour-sailserver.yaml \
    ChangeLog.txt \
    qml/pages/AboutPage.qml \
    qml/pages/CreditsModel.qml \
    qml/pages/SettingsPage.qml \
    LICENSE.txt \
    qml/pages/LicensePage.qml \
    qml/pages/ChangeLog.qml \
    doc/sailserver.tex \
    harbour-sailserver.png \
    harbour-sailserver.desktop \
    qml/harbour-sailserver.qml \
    translations/harbour-sailserver.ts \
    translations/harbour-sailserver-sv.ts \
    images/PayBtn_BgImg.png \
    css/stylesheet.css \
    translations/harbour-sailserver-de.ts \
    translations/harbour-sailserver-fi.ts \
    qml/pages/SettingsObject.qml \
    html/latest.html

HEADERS += \
    src/gen_config.h \
    src/appinfo.h \
    src/sailhttpserver.h \
    src/sailclientthread.h \
    src/permissions.h \
    src/httpbytearray.h \
    src/controllerhome.h \
    src/controllerupload.h \
    src/rpminstall.h \
    src/controllerdownload.h \
    src/controllerbase.h \
    src/controllerclipboard.h \
    src/settings.h \
    src/controllerlatest.h

RESOURCES += \
    qrc.qrc

