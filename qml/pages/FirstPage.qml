/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    id: page

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
        }

        // Tell SilicaFlickable the height of its content.
       // contentHeight: column.height

        PageHeader {
            id: header
            title: "Sailserver"
        }

        Item {
            id: column
            anchors.top: header.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: Theme.paddingLarge
            anchors.rightMargin: Theme.paddingLarge

            Label {
                id: status
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                height: Theme.itemSizeMedium
//                width: parent.width
                text: httpserver.listening ? qsTr("Server running") : qsTr("Server stopped")
                //wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeExtraLarge
                color: Theme.primaryColor
            }

            Switch
            {
                id: swit
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: status.bottom
                onClicked: httpserver.toggle()
            }

            Label {
                id: labelServer
                anchors.top: swit.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                text: "http://"+httpserver.ipAddress+":"+httpserver.port
                opacity: isRunning ? 1.0 : 0.0
                color: Theme.primaryColor
            }

            Label {
                id: labelCounter
                anchors.top: labelServer.bottom
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: isRunning ? 1.0 : 0.0
                text: qsTr("Requests: %1").arg(httpserver.connections)
            }

            Behavior on opacity {
                FadeAnimation {}
            }
        }

        Connections
        {
            target: httpserver
            onListeningChanged: swit.checked = listening
        }
    }

    Component.onCompleted: { if (httpserver.autoActivate) swit.clicked(swit) }
}


