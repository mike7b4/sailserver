import QtQuick 2.0
import Sailfish.Silica 1.0
Dialog {
    anchors.fill: parent
    onAccepted: {
                    httpserver.port = entryPort.text;
                    httpserver.allowInstall = switchAllowInstall.checked;
                    httpserver.removeRpmAfterInstall = switchRemoveRpmAfterInstall.checked;
                    httpserver.autoActivate = switchAutoActivate.checked
                    httpserver.cameraRefresh = entryRefresh.currentIndex; // as long as never is seen as 0 and  other values are incremented
                }
    DialogHeader {id: header}
    Column
    {
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: Theme.paddingSmall
        anchors.rightMargin: Theme.paddingSmall

        TextField
        {
            id: entryPort
            text: httpserver.port
            placeholderText: qsTr("Enter port > 1024")
            label: qsTr("Server Port")
            anchors.left: parent.left
            anchors.right: parent.right
        }

        TextSwitch{
            id: switchAutoActivate
            checked: httpserver.autoActivate
            text: qsTr("Activate server when app start")
            anchors.left: parent.left
            anchors.right: parent.right
        }

        TextSwitch
        {
            id: switchAllowInstall
            checked: httpserver.allowInstall
            text: qsTr("Allow install RPM's\n(Notice! OS will ask first!)")
            anchors.left: parent.left
            anchors.right: parent.right
        }

        TextSwitch
        {
            id: switchRemoveRpmAfterInstall
            visible: switchAllowInstall.checked
            text: qsTr("Remove package after install")
            checked: httpserver.removeRpmAfterInstall
            anchors.left: parent.left
            anchors.right: parent.right
        }

        ComboBox
        {
            id: entryRefresh
            currentIndex: httpserver.cameraRefresh // should be safe never will be translated to 0
            anchors.left: parent.left
            anchors.right: parent.right
            menu: ContextMenu {
                    MenuItem { text: "Never" }
                    MenuItem { text: "1" }
                    MenuItem { text: "2" }
                    MenuItem { text: "3" }
                    MenuItem { text: "4" }
                    MenuItem { text: "5" }
                    MenuItem { text: "6" }
                }
            label: qsTr("requests latest photo every(seconds):")
        }

    }
}
