import QtQuick 2.0
import QtQuick.LocalStorage 2.0
QtObject
{
    // First, let's create a short helper function to get the database connection
    property var db: undefined;
    function db_open() {
        if (db == undefined)
            db = LocalStorage.openDatabaseSync(appinfo.getName(), "1.0", "StorageDatabase", 100000);

        return db;
    }

    // At the start of the application, we can initialize the tables we need if they haven't been created yet
    function initialize() {
        db = settings_db_open();
        db.transaction(
            function(tx) {
                // Create the settings table if it doesn't already exist
                // If the table exists, this is skipped
                tx.executeSql('CREATE TABLE IF NOT EXISTS settings(setting TEXT UNIQUE, value TEXT)');
          });
    }

    // This function is used to write a setting into the database
    function set(key, value) {
       // setting: string representing the setting name (eg: “username”)
       // value: string representing the value of the setting (eg: “myUsername”)
       db_open();
       var res = "";
       db.transaction(function(tx) {
            var rs = tx.executeSql('INSERT OR REPLACE INTO settings VALUES (?,?);', [key,value]);
                  //console.log(rs.rowsAffected)
                  if (rs.rowsAffected > 0) {
                    res = "OK";
                  } else {
                    res = "Error";
                  }
            }
      );
      // The function returns “OK” if it was successful, or “Error” if it wasn't
      return res;
    }
    // This function is used to retrieve a setting from the database
    function get(key, def) {
       db_open();
       var res="";
       db.transaction(function(tx) {
         var rs = tx.executeSql('SELECT value FROM settings WHERE key=?;', [key]);
         if (rs.rows.length > 0) {
              res = rs.rows.item(0).value;
         } else {
             res = def;
         }
      })
      // The function returns “Unknown” if the setting was not found in the database
      // For more advanced projects, this should probably be handled through error codes
      return res
    }
}
