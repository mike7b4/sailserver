import QtQuick 2.0
ListModel {
    ListElement { title: "<b>Maintainers:</b>"}
    ListElement { title: "mike7b4 <mike@7b4.se>"; titleurl: "Mikael Hermansson"; url: "mailto:mike@7b4.se?subject=" }
    ListElement { title: "<b>Thanks to:</b>"}
    ListElement { title: "@Morpog for the icon"}
    ListElement { title: "<b>Credits to:</b>" }
    ListElement { title: "Jolla for the cool SailfishOS"}
    ListElement { title: "Jolla, Nemo & Qt community"}
    ListElement { title: "<b>If you like this app you may send bitcoins to:</b>" }
    ListElement { title: "1HDFaQPqXCTtRi87wJpo4w2XAwt4DadLCT"; url: "http://www.7b4.se/bitcoins"; titleurl: "1HDFaQPqXCTtRi87wJpo4w2XAwt4DadLCT"; }
}
