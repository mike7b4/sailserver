#include <QFile>
#include "httpbytearray.h"
#include "controllerupload.h"


ControllerUpload::ControllerUpload(SailClient *parent) :
    ControllerBase(parent)
{
}

QString ControllerUpload::concatFileName(QString &fname)
{
    QString filename;

    if (fname == "server.key")
    {
        filename = QDir::homePath()+"/.config/harbour-sailserver/"+fname;
    }
    else if (fname == "server.crt")
    {
        filename = QDir::homePath()+"/.config/harbour-sailserver/"+fname;
    }
    else
    {
        filename = QDir::homePath()+"/Downloads/"+fname;
    }

    return filename;
}

void ControllerUpload::parse()
{
    HTTPByteArray *http = client->getHttpObject();
    int index = http->getData().indexOf("\r\n\r\n");
    if (index >= 0)
    {
        QString filename;
        QString bound;
        QString str = QString(http->getData().left(index));
        QByteArray data = http->getData().remove(0, index+4);
        QStringList content = str.split("\r\n");
        foreach(str, content)
        {
            qDebug() << str << " vs " << http->getHeader("boundary");
            if (str.indexOf(http->getHeader("boundary")) >= 0)
            {
                bound = str;
                qDebug() << data << " indexof " << data.indexOf(bound);
                data = data.left(data.indexOf("\r\n"+bound)); // seek for end...
            }
            else if (str.startsWith("Content-Disposition: form-data; name=\"filename\"; filename="))
            {
                filename = str.replace("Content-Disposition: form-data; name=\"filename\"; filename=\"", "");
                filename = filename.replace("\"", "");
                filename = filename.replace("..", "");
                filename = filename.replace("/", "");
                qDebug() << filename;
            }
        }

        QFile file(concatFileName(filename));
        if (file.exists())
        {
            client->responseOK();
            client->beginHtml(tr("File already exists :("));
            client->appendH1(tr("Upload failed :("));
            client->appendBodyS(QString(tr("'%1' already exists!").arg(filename)+"<FORM METHOD=\"POST\" enctype=\"multipart/form-data\"><INPUT TYPE=\"file\" size=\"10000000\" name=\"filename\"><INPUT TYPE=\"submit\" name=\"submit\" value=\"send\"></FORM>"));
            client->appendBodyS(QString("<hr/><a href=\"/home\">"+tr("Home")+"</a>\n"));
            client->endHtml();
            client->close();
            client->getServer()->message(tr("File '%1' already exists!").arg(filename));
        }
        else if(file.open(QIODevice::WriteOnly))
        {
            file.write(data);
            file.close();
            client->responseOK();
            client->beginHtml(tr("Upload succeed :)"));
            client->appendH1(tr("Upload succeed :)"));
            client->appendBody("<FORM METHOD=\"POST\" enctype=\"multipart/form-data\"><INPUT TYPE=\"file\" size=\"10000000\" name=\"filename\"><INPUT TYPE=\"submit\" name=\"submit\" value=\"send\"></FORM>");
            client->appendBodyS(QString("<hr/><a href=\"/home\">"+tr("Home")+"</a>\n"));
            client->endHtml();
            client->close();
            client->getServer()->message(tr("%1 uploaded :)").arg(filename));
            if (client->getServer()->property("allowInstall").toBool() && filename.endsWith(".rpm"))
            {
                client->getServer()->rpmInstall(file.fileName());
            }
        }
        else
        {
            client->responseOK();
            client->beginHtml(tr("Could not create file :("));
            client->appendH1(tr("Upload failed! Could not create '%1' :(").arg(filename));
            client->appendBodyS("<FORM METHOD=\"POST\" enctype=\"multipart/form-data\"><INPUT TYPE=\"file\" size=\"10000000\" name=\"filename\"><INPUT TYPE=\"submit\" name=\"submit\" value=\"send\"></FORM>");
            client->appendBodyS(QString("<hr/><a href=\"/home\">"+tr("Home")+"</a>\n"));
            client->endHtml();
            client->close();
            client->getServer()->message(tr("Failed to create\n'%1' :/").arg(filename));
        }
    }
}

void ControllerUpload::handleResponse()
{
    HTTPByteArray *http = client->getHttpObject();
    if (http->getHeader("method") == "GET")
    {
        client->responseOK();
        client->beginHtml(tr("Upload"));
        appendMenu(tr("Home"), "home");
        appendMenu(tr("Upload"),  "");
        appendMenu(tr("Clipboard"),  "clipboard");
        mergeMenus();

        sendData(tr("Upload"), false);
        client->appendH1(tr("Upload"));
        client->appendBodyS(tr("(Max filesize is 10Mb)"));
        client->appendBR();
        if (!client->isEncrypted())
        {
            client->appendBody("<p>");
            client->appendBodyS(tr("If you want to secure the server you can upload a server.key and server.crt and it will be automatic stored in .config/harbour-sailserver/ directory and after that you can use https://%1 instead of http://%2").arg(http->getHeader("Host")).arg(http->getHeader("Host")));
            client->appendBody("</p>");
        }
        client->appendBody("<FORM METHOD=\"POST\" enctype=\"multipart/form-data\"><INPUT TYPE=\"file\" name=\"filename\" size=\"10000000\"><INPUT TYPE=\"submit\" name=\"submit\" value=\"send\"></FORM>");
        client->endHtml();
    }
    else
    {
        parse();
    }
}

