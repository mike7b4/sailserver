#include "sailhttpserver.h"
#include "sailclientthread.h"
#include <QDebug>


/** \brief Constructor set local ipAddress and initialize */
SailHttpServer::SailHttpServer(AppInfo *app) :
    QTcpServer(app),
    appinfo(app),
    ipAddress(""),
    latestPicture(""),
    autoActivate(false),
    _port(0),
    _connections(0),
    settings(this),
    _permissions(this),
    pRpmInstall(NULL),
    filewatcherPictures(app)
{
    _port = settings.value("Config/ServerPort", QVariant(8080)).toInt();
    autoActivate = settings.value("Config/AutoActivate", QVariant(false)).toBool();
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty()) {
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    }

    QFile file(appinfo->getConfigPath()+"/stylesheet.css");
    qDebug() << file.fileName();
    if (!file.exists() && file.open(QFile::WriteOnly)) // if stylesheet not exist
    {
        QFile copy("/usr/share/harbour-sailserver/css/stylesheet.css");
        if (copy.open(QFile::ReadOnly))
        {
            file.write(copy.readAll());
            copy.close();
            file.close();
        }
    }

    QFile fcamera(appinfo->getConfigPath()+"/latest.html");
    if (!fcamera.exists() && fcamera.open(QFile::WriteOnly)) // if camera.html not exist
    {
        QFile copy("/usr/share/harbour-sailserver/html/latest.html");
        if (copy.open(QFile::ReadOnly))
        {
            fcamera.write(copy.readAll());
            copy.close();
            fcamera.close();
        }
    }

    QDir dir(QDir::homePath()+"/Pictures/Camera/");
    QStringList files = dir.entryList(QDir::Files | QDir::Readable, QDir::Time);
    qDebug() << files;
    if (files.size())
        latestPicture = files.at(0);

    filewatcherPictures.addPath(QDir::homePath()+"/Pictures/Camera");
    connect(&filewatcherPictures, SIGNAL(directoryChanged(const QString&)), this, SLOT(onPictureDirectoryChanged(const QString&)));
}

/** slot client connection called by TcpServer when client has connected
    FIXME makes clients as Threads
*/
void SailHttpServer::incomingConnection(int socket)
{
//    QTcpSocket *socket = nextPendingConnection();
    SailClientThread *client = new SailClientThread(this, QString(ipAddress+":%1").arg(_port), socket);
    if (client != NULL)
    {
        //clients.append(client);
        QThreadPool::globalInstance()->start(client);
        _connections++;
        emit connectionsChanged(_connections);
    }
}

/** \brief toggle server on/off the function will emit connected or disconneced
 * \param toggle true if start false stop
 */
void SailHttpServer::toggle()
{
    if (!isListening())
    {
        qDebug() << "listen on Ip: " << ipAddress;
        listen(QHostAddress(ipAddress), _port);
    }
    else
    {
        close();
    }

    emit listeningStateChange(isListening());
}

void SailHttpServer::setAllowInstall(bool allow)
{
    settings.setValue("Config/AllowInstall", QVariant(allow));
    emit allowInstallChanged(allow);
}

bool SailHttpServer::getAllowInstall()
{
    return settings.value("Config/AllowInstall", QVariant(true)).toBool();
}

void SailHttpServer::setRemoveRpmAfterInstall(bool value)
{
    settings.setValue("Config/RemoveRpmAfterInstall", QVariant(value));
    emit removeRpmAfterInstallChanged(value);
}

void SailHttpServer::setIpAddress(QString ip)
{
    emit ipAddressChanged(ip);
}

void SailHttpServer::setCameraRefresh(int value)
{
    settings.setValue("Config/CameraRefresh", QVariant(value));
    emit cameraRefreshChanged();
}

int SailHttpServer::getCameraRefresh()
{
    return settings.value("Config/CameraRefresh").toInt();
}

void SailHttpServer::setAutoActivate(bool value)
{
    settings.setValue("Config/AutoActivate", QVariant(value));
    autoActivate = value;
    emit autoActivateChanged(value);
}

bool SailHttpServer::getRemoveRpmAfterInstall()
{
    return settings.value("Config/RemoveRpmAfterInstall", QVariant(true)).toBool();
}

void SailHttpServer::rpmInstall(QString installrpm)
{
    if (pRpmInstall == NULL)
    {
        pRpmInstall = new RpmInstall(this);
    }

    pRpmInstall->run(installrpm);
}

void SailHttpServer::onPictureDirectoryChanged(const QString &path)
{
    QDir dir(QDir::homePath()+"/Pictures/Camera");
    QStringList files = dir.entryList(QDir::Files | QDir::Readable, QDir::Time);
    if (files.size())
        latestPicture = files.at(0);
}
