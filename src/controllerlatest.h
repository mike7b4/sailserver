#ifndef CONTROLLERREFRESH_H
#define CONTROLLERREFRESH_H
#include "sailhttpserver.h"
#include "sailclientthread.h"
#include "controllerbase.h"
class ControllerLatest : public ControllerBase
{
    Q_OBJECT
public:
    explicit ControllerLatest(SailClient *parent);
    void handleResponse();
signals:

public slots:

};

#endif // CONTROLLERREFRESH_H
