#include "permissions.h"

Permissions::Permissions(QObject *parent) :
    QObject(parent),
    allowedPaths()
{
    allowedPaths["pictures"] = new Path("Pictures", false);
    allowedPaths["Pictures"] = new Path("Pictures", false);
    allowedPaths["music"] = new Path("Music", false);
    allowedPaths["Music"] = new Path("Music", false);
    allowedPaths["Documents"] = new Path("Documents", false);
    allowedPaths["documents"] = new Path("Documents", false);
    allowedPaths["Videos"] = new Path("Videos", false);
    allowedPaths["Downloads"] = new Path("Downloads", true);
}

Permissions::~Permissions()
{
    // dirty
    foreach (Path *ptr, allowedPaths)
    {
        delete ptr;
    }
}

void Permissions::addAllowedPath(QString symlink, QString realname, bool write)
{
    // notice this may overtwrite
    allowedPaths[symlink] = new Path(realname, write);
}

bool Permissions::isAllowedPath(QString suburl)
{
    return allowedPaths.contains(suburl);
}

bool Permissions::isAllowedWritePath(QString suburl)
{
    if (allowedPaths.contains(suburl))
    {
        return allowedPaths[suburl]->writeable;
    }

    return false;
}

QString Permissions::getRealPath(QString suburl)
{
    if (allowedPaths.contains(suburl))
    {
        return allowedPaths[suburl]->path;
    }
    else
    {
        return "";
    }
}
