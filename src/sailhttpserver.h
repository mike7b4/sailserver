#ifndef SAILHTTPSERVER_H
#define SAILHTTPSERVER_H

#include <QTcpServer>
#include <QtNetwork>
#include <QList>
#include <QThreadPool>
#include <QFileSystemWatcher>
#include "settings.h"
#include "permissions.h"
#include "rpminstall.h"
#include "appinfo.h"
class SailHttpServer : public QTcpServer
{
    Q_OBJECT
   // QList <QObject*>clients;
    AppInfo *appinfo;
    QString ipAddress;
    QString latestPicture; // holds latest picture taken
    bool autoActivate;
    int _port;
    int _connections;
    Settings settings;
    Permissions _permissions;
    RpmInstall *pRpmInstall;
    QFileSystemWatcher filewatcherPictures;
    Q_PROPERTY (int connections READ getConnections NOTIFY connectionsChanged)
    Q_PROPERTY (int port READ getPort WRITE setPort NOTIFY portChanged)
    Q_PROPERTY (bool allowInstall READ getAllowInstall WRITE setAllowInstall NOTIFY allowInstallChanged)
    Q_PROPERTY (bool removeRpmAfterInstall READ getRemoveRpmAfterInstall WRITE setRemoveRpmAfterInstall NOTIFY removeRpmAfterInstallChanged)
    Q_PROPERTY (QString ipAddress READ getIpAddress WRITE setIpAddress NOTIFY ipAddressChanged)
    Q_PROPERTY (bool listening READ isListening NOTIFY listeningStateChange)
    Q_PROPERTY (bool autoActivate READ getAutoActivate WRITE setAutoActivate NOTIFY autoActivateChanged)
    Q_PROPERTY (int cameraRefresh READ getCameraRefresh WRITE setCameraRefresh NOTIFY cameraRefreshChanged)
public:
    explicit SailHttpServer(AppInfo *parent = 0);
    Permissions & getPermissions() { return _permissions;};
    void rpmInstall(QString rpm);
    QString &getLatestPicture(){return latestPicture; };
    int getCameraRefresh();
    AppInfo *getAppInfo(){ return appinfo; };
protected:
    QString getIpAddress(){return ipAddress;}
    int getPort(){return this->_port;};
    int getAutoActivate(){ return this->autoActivate; };
    void setAutoActivate(bool set);
    bool getAllowInstall();
    void setAllowInstall(bool allow);
    bool getRemoveRpmAfterInstall();
    void setRemoveRpmAfterInstall(bool value);
    void setIpAddress(QString ip);
    void setCameraRefresh(int value);
    void setPort(int port){ this->_port = port;  settings.setValue("Config/ServerPort", port); emit portChanged(port); if (isListening()){toggle(); toggle();}}
    int getConnections(){ return _connections; };
    void incomingConnection(int socket);
signals:
    void listeningStateChange(bool listening);
    void portChanged(int port);
    void connectionsChanged(int connections);
    void allowInstallChanged(bool allowInstall);
    void ipAddressChanged(QString ip);
    void autoActivateChanged(bool active);
    void removeRpmAfterInstallChanged(bool removeRpmAfterInstall);
    void message(QString message);
    void cameraRefreshChanged();
public slots:
    void toggle();
    void onPictureDirectoryChanged(const QString &path);

};

#endif // SAILHTTPSERVER_H
