#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QUrl>
#include <QJsonArray>
#include <QJsonObject>
#include "sailclientthread.h"
#include "controllerdownload.h"
#include "sailhttpserver.h"
#include "httpbytearray.h"
#include "controllerhome.h"

ControllerHome::ControllerHome(SailClient *parent) :
    ControllerDownload(parent)
{
}

void ControllerHome::handleResponse()
{
    HTTPByteArray *http = client->getHttpObject();
    QString path = http->getPath(); // controller splitted away
    qDebug() << path;
    if (function == "")
    {
        listFiles("", "");
    }
    else if (function == "harbour-sailserver")
    {
        QString concat = QDir::homePath()+"/.config/harbour-sailserver/"+QUrl(path).toString();
        downloadFile(concat);
    }
    else if (client->getServer()->getPermissions().isAllowedPath(function))
    {
        QString concat = QDir::homePath()+"/"+client->getServer()->getPermissions().getRealPath(function)+"/"+QUrl(path).toString();
        QDir dir(concat);
        QFile fn(concat);
        qDebug() << concat;
        // is_dir?
        if(dir.exists())
        {
            // if user has a dir with foo.json this breaks but... normally we expect .json extension to mean that we want to list files/dirs in json format
            listFiles(function, concat);
        }
        else if(fn.exists())
        {
            downloadFile(concat);
        }
        else
        {
            client->notFound(concat);
        }
    }
    else
    {
        client->denied();
    }
}

void  ControllerHome::listFiles(QString title, QString path)
{
    HTTPByteArray *http = client->getHttpObject();
    QJsonObject obj;
    QDir dir;
    QString extracontent;
    // we allow pictures and Pictures...
    if (title == "") // root eg /home/nemo
    {
        appendMenu(tr("Home"), "");
        appendMenu(tr("Upload"), proto+http->getHeader("Host")+"/upload");
        appendMenu(tr("Clipboard"), QUrl(proto+http->getHeader("Host")+"/clipboard").toString());
        appendMenu(tr("Latest shot"), QUrl(proto+http->getHeader("Host")+"/latest/camera.html").toString());
        mergeMenus();
        title = "home";
        append("<table>");
        append("<tr><th>"+title+"</th></tr>");

        append("<tr><td>");
        append(tr("Documents"), proto+http->getHeader("Host")+"/home/Documents", "");
        append("</td></tr>");

        append("<tr><td>");
        append(tr("Music"), proto+http->getHeader("Host")+"/home/Music", "");
        append("</td></tr>");

        append("<tr><td>");
        append(tr("Pictures"), proto+http->getHeader("Host")+"/home/Pictures", "");
        append("</td></tr>");

        append("<tr><td>");
        append(tr("Videos"), proto+http->getHeader("Host")+"/home/Videos", "");
        append("</td></tr>");

        append("<tr><td>");
        append(tr("Downloads"), proto+http->getHeader("Host")+"/home/Downloads", "");
        append("</td></tr>");

        append("<tr><td>");
        append(tr("SDCard"), proto+http->getHeader("Host")+"/media/sdcard", "");
        append("</td></tr>");

        append("</table>");

        extracontent +="<hr />";
        extracontent +="<form name=\"input\" action=\"/clipboard/set/\"  method=\"GET\">"+tr("Set clipboard: ")+"<input type=\"text\" name=\"data\"><br><input type=\"submit\" value=\"Set\"></form>";
    }
    else
    {
        ControllerDownload::listFiles(title, path);
        return ;
    }

    sendData(title);
}

