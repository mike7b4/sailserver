#ifndef CONTROLLERCLIPBOARD_H
#define CONTROLLERCLIPBOARD_H
#include "controllerbase.h"
class ControllerClipboard : public ControllerBase
{
    Q_OBJECT
public:
    explicit ControllerClipboard(SailClient *parent);
    void handleRequest();
signals:

public slots:

};

#endif // CONTROLLERCLIPBOARD_H
