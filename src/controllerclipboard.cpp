#include <QGuiApplication>
#include <QClipboard>
#include <QUrlQuery>
#include "controllerclipboard.h"

ControllerClipboard::ControllerClipboard(SailClient *parent) :
    ControllerBase(parent)
{
}

void ControllerClipboard::handleRequest()
{
    HTTPByteArray *http = client->getHttpObject();
    if (http->getFunction()=="set")
    {
        try {
            // get POST <textbox> data
            QString data = QUrlQuery(http->getData()).queryItemValue("data");
            // QUrl does not convert + sign to space(%20)
            data = data.replace("+", "%20");
            // convert to normal utf8 string
            data = QString(QByteArray::fromPercentEncoding(data.toUtf8()));
            // ... and set jolla clipboard
            qApp->clipboard()->setText(data);

            client->responseOK();
            client->beginHtml(tr("Sailserver - Clipboard updated"), QString("<meta http-equiv=\"refresh\" content=\"1; url="+proto+http->getHeader("Host")+"/home/\" />"));
            client->appendH1(tr("Clipboard updated :)"));
            client->endHtml();

            emit client->getServer()->message(tr("Clipboard updated"));
        }
        catch(...)
        {
            client->badRequest();
        }
    }
    else
    {
        appendMenu(tr("Home"), "home");
        appendMenu(tr("Upload"), "upload");
        appendMenu(tr("Clipboard"),  "");
        mergeMenus();
//        append("<form name=\"input\" action=\"/clipboard/set/\"  enctype=\"application/x-www-form-urlencoded\" method=\"POST\">"+tr("Set clipboard: ")+"<input type=\"text\" name=\"data\"><br><input type=\"submit\" value=\"Set\"></form>");
        append("<form name=\"input\" action=\"/clipboard/set/\"  enctype=\"application/x-www-form-urlencoded\" method=\"POST\">"+tr("Set clipboard: ")+"<br /><textarea name=\"data\" rows='5' column='50'></textarea><br /><input type=\"submit\" value=\"Set clipboard\"></form>");
        sendData("Clipboard");
    }
}
