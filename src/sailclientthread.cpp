#include <QObject>
#include <QStringList>
#include <QGuiApplication>
#include <QClipboard>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QByteArrayMatcher>
#include <QMimeDatabase>
#include <QDebug>
#include "sailhttpserver.h"
#include "sailclientthread.h"
#include "controllerclipboard.h"
#include "controllerhome.h"
#include "controllerupload.h"
#include "controllerlatest.h"

SailClientThread::SailClientThread(SailHttpServer *serv, QString hos, int socket)
    :QRunnable(),
    pServer(serv),
    host(hos),
    socketDescriptor(socket),
    client(NULL)
{

}

void SailClientThread::run()
{
    QFile file;
    QSslSocket socket;
    socket.setSocketDescriptor(socketDescriptor);

    client = new SailClient((QObject*)NULL, pServer, host, &socket);
    if (file.exists(QDir::homePath()+"/.config/harbour-sailserver/server.crt") &&
        file.exists(QDir::homePath()+"/.config/harbour-sailserver/server.key"))
    {
        socket.setLocalCertificate(QDir::homePath()+"/.config/harbour-sailserver/server.crt");
        socket.setPrivateKey(QDir::homePath()+"/.config/harbour-sailserver/server.key");
        qDebug() << "d" << socket.localCertificate();
        socket.startServerEncryption();
        if (socket.waitForEncrypted())
            client->onEncrypted();
        else
            qDebug() << socket.errorString();
    }

    while(socket.isOpen())
    {
        if (socket.waitForReadyRead(100))
        {
            qDebug() << "dododo";
            client->onReadyRead();
        }
    }
    qDebug() << "thread died";
}

SailClient::SailClient(QObject *parent, SailHttpServer *serv, QString host, QSslSocket *socket) :
    QObject(parent),
    host(host),
    http(),
    pTcpSocket(socket),
    pServer(serv)
{
    //connect(pTcpSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(pTcpSocket, SIGNAL(sslErrors(const QList<QSslError>&)), this, SLOT(onSslErrors(const QList<QSslError>&)));
    connect(pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
    connect(pTcpSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

void SailClient::onDisconnected()
{
    pTcpSocket->close();
    qDebug() << "die";
}

void SailClient::onEncrypted()
{
    qDebug() <<  "onencrypted";
}

void SailClient::onError(QAbstractSocket::SocketError err)
{
    switch(err)
    {
        case QAbstractSocket::SocketTimeoutError:
            //if (pTcpSocket->bytesToWrite() == 0)
            //    pTcpSocket->close();
            if (pTcpSocket->flush())
                qDebug() << "flush";
            break;
        case QAbstractSocket::RemoteHostClosedError:
            pTcpSocket->close();
            qDebug() << err;
            break;
        default:
            break;
    }
}

void SailClient::onSslErrors (const QList<QSslError> &errors )
{
    qDebug() << errors;
}


void SailClient::responseControl()
{
    QString proto = isEncrypted() ? "https://" : "http://";
    if (http.getController() == "clipboard")
    {
        ControllerClipboard clip(this);
        clip.handleRequest();
    }
    else if (http.getController() == "home")
    {
        ControllerHome home(this);
        home.handleResponse();
    }
    else if (http.getController() == "upload") // uploads/directory
    {
        ControllerUpload control(this);
        control.handleResponse();
    }
    else if (http.getController() == "media") // sdcard
    {
        ControllerDownload control(this);
        control.handleResponse();
    }
    else if (http.getController() == "latest") // show latest photo
    {
        ControllerLatest control(this);
        control.handleResponse();
    }
    else if (http.getController() == ".config") // downloads/directory
    {
        ControllerHome control(this);
        control.handleResponse();
    }
    else
    {
        badRequest();
    }
}

/* when there is data comming from client */
void SailClient::onReadyRead()
{
    QString proto = isEncrypted() ? "https://" : "http://";
    HTTPByteArray::HttpResponse result;
    receivedData = pTcpSocket->readAll();

    qDebug() << "readlength: " << receivedData.length();
    if (!http.hasHeader())
        qDebug() << receivedData;

    result = http.append(receivedData);
    receivedData.clear();
    switch(result)
    {
        case HTTPByteArray::HTTP_INVALID_REQUEST:
            badRequest();
            break;
        case HTTPByteArray::HTTP_OK:
            responseControl();
            close();
            break;
        case HTTPByteArray::HTTP_MEMORY_FULL:
            responseOK();
            beginHtml(tr("The file was too big :("));
            appendBody("<h1>To big file upload aborted</h1>\r\n\0");
            endHtml();
            emit pServer->message(tr("File was too big aborted"));
            break;
        case HTTPByteArray::HTTP_REDIRECT:
            responseOK();
            beginHtml(tr("Sailserver - Redirect to home"), QString("<meta http-equiv=\"refresh\" content=\"1; url="+proto+http.getHeader("Host")+"/home/\" />"));
            appendH1(tr("Redirect to home in a 1 second"));
            endHtml();
            break;
        case HTTPByteArray::HTTP_PENDING:
            return;
        default:
            break;
    }

    http.clear();
}

void SailClient::parsePOST(QStringList list)
{
    bool postok = false;
    int contentLength = 0;
    foreach (QString line, list)
    {
        if (line.startsWith("Content-Length: "))
        {
            contentLength = line.split("Content-Length: ")[1].toInt();
            qDebug() << contentLength;
        }
        else if (line.startsWith("Content-Type: multipart/form-data"))
        {
            qDebug() << line;
            postok = true;
        }
    }

    if (postok && contentLength)
    {
        receivedData = pTcpSocket->readAll();
        responseCONTINUE();
        while (!receivedData.length())
        {
            QThread::sleep(1);
            receivedData = pTcpSocket->readAll();
            qDebug() << "dow" ;
        };
        qDebug() << "more" << receivedData.length();
        responseOK(true);
        beginHtml("FIXME redirect");
        write("<h1>FIXME redirect</h1>");
        endHtml();
        close();
    }
    else {
        badRequest();
    }
}

void SailClient::notFound(QString str)
{
    write(QString("HTTP/1.0 404 Not found %1\r\n"
                  "Content-Type: text/html; charset=\"utf-8\"\r\n"
                  "Content-Length: %1\r\n"
                  "Connection: Close\r\n"
                  "\r\n").arg(str.size()).toUtf8());
    write(str.toUtf8());
}

void SailClient::badRequest(QString str)
{
    write(QString("HTTP/1.0 400 Bad Request\r\n"
                  "Content-Type: text/html; charset=\"utf-8\"\r\n"
                  "Content-Length: %1\r\n"
                  "Connection: Close\r\n"
                  "\r\n").arg(str.size()).toUtf8());
    write(str.toUtf8());
}

void SailClient::forbidden(QString str)
{
    write("HTTP/1.0 403 Forbidden\r\n");
    write(QString("Content-Type: text/html; charset=\"utf-8\"\r\n"
          "Content-Length: %1\r\n"
                  "Connection: Close\r\n"
                  "\r\n").arg(str.size()).toUtf8());
    write(str.toUtf8());
}

void SailClient::denied(QString str)
{
    write(QString("HTTP/1.0 401 Denied\r\n"
                  "Content-Type: text/html; charset=\"utf-8\"\r\n"
                  "Content-Length: %1\r\n"
                  "Connection: Close\r\n"
                  "\r\n").arg(str.size()).toUtf8());
    write(str.toUtf8());
}

void SailClient::beginHtml(QString title, QString metadata, QString extrahead)
{
    appendBody("<html><head><title>"+title.toUtf8()+"</title>"+metadata.toUtf8()+"\n");
    appendBody("<LINK href=\"/.config/harbour-sailserver/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\">");
    appendBodyS(extrahead);
    appendBody("</head><body>\n");
}

void SailClient::endHtml(bool addendtag)
{
    if (addendtag)
        appendBody("</body></html>\r\n");

    body.prepend(QString("%1\r\n").arg(body.size(), 0, 16).toUtf8());
    body.append("\r\n0\r\n\r\n");
    write(body);
    qDebug() << body;
    body.clear();
    close();
}

void SailClient::appendBody(const QByteArray &data)
{
    body.append(data);
}

void SailClient::responseOK(bool endhead, bool chunkbody, QString ctype)
{
    write(QString("HTTP/1.1 200 OK\r\nServer: Sailserver\r\nConnection: close\r\nContent-Type: %1\r\n").arg(ctype).toUtf8());
    if (chunkbody)
    {
        write("Transfer-Encoding: chunked\r\n");
    }
    if (endhead) {
        write("\r\n");
    }
}

void SailClient::responseCONTINUE()
{
    write("HTTP/1.1 100 CONTINUE\r\n");
}

void SailClient::write(const QByteArray &data)
{
    pTcpSocket->write(data);
   // pTcpSocket->flush();
    qDebug() << "send: === " << data;
   // qDebug() << pTcpSocket->waitForBytesWritten();
    qDebug() << "done";
}

void SailClient::close()
{
    if (pTcpSocket->isOpen())
    {
     //   pTcpSocket->flush();
  //      pTcpSocket->waitForBytesWritten(-1);
        //sleep(1);
    //    pTcpSocket->close();
    }
}
