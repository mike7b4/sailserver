#include "controllerbase.h"

ControllerBase::ControllerBase(SailClient *parent) :
    QObject(parent)
{
    client = parent;
    proto = client->isEncrypted() ? "https://" : "http://";
    function = client->getHttpObject()->getFunction();
    controller = client->getHttpObject()->getController();
}

void ControllerBase::append(QString title, QString url, bool isfile, QString endtag)
{
    QJsonObject obj;
    if (url != "") {
        obj["url"] = url;
        obj["isFile"] = isfile;
    }
    if (endtag != "")
        obj["endtag"] = endtag;

    obj["title"] = title;
    array.append(obj);
}

void ControllerBase::appendMenu(QString title, QString url)
{
    myMapType obj;
    obj["title"] = title;
    if (url == "")
    {
        menus.append(obj);
        return ;
    }

    if (!url.contains("http")) // relative?
    {
        // yes add missing http...
        url = QUrl(proto+client->getHttpObject()->getHeader("Host")+"/"+url).toString();
    }

    obj["url"] = url;
    menus.append(obj);
}

void ControllerBase::mergeMenus()
{
    append("<div class='panel'>");
    foreach (myMapType h, menus)
    {
        append("<span class='button'>");
        if (h.contains("url"))
        {
            append(h["title"], h["url"], false, "");
        }
        else
        {
            append(h["title"], "", false, "");
        }
        append("</span>");
    }
    append("<span class='logo'>Sailserver</span>");
    append("</div>");
}

void ControllerBase::sendData(QString title, bool all)
{
    if (!client->getHttpObject()->wantJSONResponse())
    {
        if (all)
        {
            client->responseOK();
            client->beginHtml(tr("Sailserver - %1").arg(title));
        }

        foreach (const QJsonValue &value, array){
            QJsonObject ob = value.toObject();
            if (ob.contains("url"))
            {
                client->appendBodyS(("<a href=\""+ob["url"].toString()+"\">"+ob["title"].toString()+"</a>"));
            }
            else
            {
                client->appendBodyS((ob["title"].toString()));
            }

            if (ob.contains("endtag"))
            {
                client->appendBodyS((ob["endtag"].toString()+"\n"));
            }
        }

        if (all)
        {
            client->appendBodyS(extracontent);
            client->endHtml();
        }
    }
    else
    {
        QJsonObject obj = QJsonObject();
        QJsonArray urls;
        /* i hate this copy but ... */
        foreach (const QJsonValue &value, array){
            QJsonObject ob = value.toObject();
            if (ob.contains("url"))
            {
                QJsonObject o;
                o["url"] = ob["url"];
                o["isFile"] = ob["isFile"];
                urls.append(o);
            }
        }
        obj["urls"] = urls;
        QJsonDocument doc(obj);
        client->responseOK(false, false, "application/json");
        client->write(QString("Content-Length: %1\r\n\r\n").arg(doc.toJson().size()).toUtf8());
        qDebug() << doc.toJson();
        client->write(doc.toJson());
    }

}

void ControllerBase::parsePost()
{
    HTTPByteArray *http = client->getHttpObject();
    int index = http->getData().indexOf("\r\n\r\n");
    if (index >= 0)
    {
        QString filename;
        QString bound;
        QString str = QString(http->getData().left(index));
        data = http->getData().remove(0, index+4);
        QStringList content = str.split("\r\n");
        foreach(str, content)
        {
            qDebug() << str << " vs " << http->getHeader("boundary");
            if (str.indexOf(http->getHeader("boundary")) >= 0)
            {
                bound = str;
                qDebug() << data << " indexof " << data.indexOf(bound);
                data = data.left(data.indexOf("\r\n"+bound)); // seek for end...
            }
            else if (str.startsWith("Content-Disposition: form-data; name=\"filename\"; filename="))
            {
                filename = str.replace("Content-Disposition: form-data; name=\"filename\"; filename=\"", "");
                filename = filename.replace("\"", "");
                filename = filename.replace("..", "");
                filename = filename.replace("/", "");
                qDebug() << filename;
            }
        }

        qDebug() << "data: " << data;
    }
}
