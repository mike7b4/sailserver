#include <QDebug>
#include <QtDBus/QDBusConnection>
#include <QFile>
#include <QUrl>
#include <QDesktopServices>
#include "sailhttpserver.h"
#include "rpminstall.h"

RpmInstall::RpmInstall(QObject *parent) :
    QObject(),
    installFilename("")
{
    this->moveToThread(parent->thread());
    setParent(parent);
    if (!QDBusConnection::sessionBus().connect("com.jolla.jollastore", "/StoreClient", "com.jolla.jollastore" ,"packageStatusChanged", this, SLOT(onPackageStatusChanged(QString,int))))
    {
        qDebug() << "Could not connect to Store " << QDBusConnection::sessionBus().lastError().message();
    }
}

void RpmInstall::run(QString installpackage)
{
    installFilename = installpackage;
    QDesktopServices::openUrl(QUrl(installFilename));
}

void RpmInstall::onPackageStatusChanged(const QString &filename, int status)
{
    SailHttpServer *server = dynamic_cast <SailHttpServer *>(parent());
    if (installFilename != filename)
    {
//        qDebug() << "install (not mine ignored) " << filename << " status " << status;
        return ;
    }

    qDebug() << "install " << filename << " status " << status;
    if (status == 1) // installed
    {
        if (server && server->property("removeRpmAfterInstall").toBool())
        {
            QFile file(filename);
            file.remove();
        }
    }
}
