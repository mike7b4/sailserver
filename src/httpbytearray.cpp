#include <QByteArray>
#include <QStringList>
#include <QRegExp>
#include <QDebug>
#include "httpbytearray.h"

HTTPByteArray::HTTPByteArray(QObject *parent) :
    QObject(parent),
    hmap(),
    args(),
    header(),
    data(),
    controller(),
    function(),
    headerIsReady(false),
    contentLength(0),
    multiContentLength(0)
{
}

void HTTPByteArray::printDebug()
{
    qDebug() << "Header is Ready: " << headerIsReady;
    qDebug() << hmap;
    if (headerIsReady)
    {
        qDebug() << "Controller: " << controller;
        qDebug() << "Function: " << function;
        qDebug() << "args" << args;
        qDebug() << data;
    }
}

void HTTPByteArray::parseHeaderLines(QStringList &list)
{
    qDebug() << list;
    foreach (QString s, list)
    {
        QStringList l = s.split(": "); // FIXME better use regexp here
        if (l.length() > 1)
        {
            QString key = l[0];
            l.removeAt(0); // get rid of key....
            QString value = l.join(": "); // FIXME check RFC... will this ever happen that we have whitespace AND : as value?
            if (key == "Content-Length")
            {
                hmap["Content-Length"] = QVariant(value);
            }
            else if (key == "Host")
            {
                hmap["Host"] = QVariant(value);
            }
            else if (key == "Content-Disposition")
            {
                hmap["Content-Disposition"] = QVariant(value);
                QRegExp rxlen("* filename=*");
                if(rxlen.indexIn(value) != -1)
                {
                    hmap["filename"] = QVariant(rxlen.cap(0));
                }
            }
            else if (key == "Content-Type")
            {
                hmap["Content-Type"] = QVariant(value);
                value = value.split("multipart/form-data; boundary=", QString::SkipEmptyParts)[0];
                if (value.size())
                    hmap["boundary"] = value;
            }
            else
            {
                hmap["?"+key] = QVariant(value);
            }
        }
    }
}

HTTPByteArray::HttpResponse HTTPByteArray::append(QByteArray &arr)
{
    int index = arr.indexOf("\r\n\r\n");
    if (index >= 0 && !headerIsReady)
    {
        QStringList cfa; // control, function arg(s)
        header.append(arr.left(index));
        data.clear();
        arr = arr.remove(0, index+4);
        controller = "";
        function = "";
        args.clear();
        args["json"] = false;
        hmap.clear();
        hmap["url"] = "";
        hmap["Content-Length"] = 0;
        hmap["Host"] = "";
        QStringList list =  QString(header).split("\r\n");
        if (list.length() == 0 || !list[0].endsWith("HTTP/1.1") || !(list[0].startsWith("POST ") || list[0].startsWith("GET ")))
        {
            qDebug() << "GET/POST wrong" << list[0];
            return HTTP_INVALID_REQUEST;
        }

        hmap["method"] = list[0].startsWith("POST ") ? "POST" : "GET";
        QString url = list[0].split(" ")[1].replace("..", "");
        args["json"] = url.endsWith(".json");
        url = url.replace(".json", ""); // FIXME if file is a .json file we can download it since we thinks we requests json response, we should handle this better...
        list.removeAt(0); // get rid of first line
        parseHeaderLines(list);
        cfa = url.split("/", QString::SkipEmptyParts);
        if (!cfa.size())
        {
            printDebug();
            return HTTP_REDIRECT;
        }

        controller = cfa[0];
        if (cfa.size() == 2)
        {
            function = cfa[1];
            hmap["url"] = "";
        }
        else if(cfa.size() > 2)
        {
            function = cfa[1];
            cfa.removeAt(0);
            cfa.removeAt(0);
            cfa = cfa.join("/").split("?"); // join / again and then split args eg [foo, bar?arg=1&arg2=2] => [foo/bar,?arg1&arg2=2]
            hmap["url"] = cfa[0]; // first is path
            cfa.removeAt(0); // remove path element
            if (cfa.size())
            {
                cfa = cfa[0].split("&"); // split the rest (if exist)
            }

            foreach (QString s, cfa)
            {
                QStringList kv = s.split("=");
                if (kv.size() > 1 && s != "url") // ignore args that is named path
                {
                    args[kv[0]] = kv[1]; // only key value pairs are stored
                }
            }
        }

        qDebug() << args;
        headerIsReady = true;
        header.clear();
    }
    else if(!headerIsReady)
    {
        header.append(arr);
        return HTTPByteArray::HTTP_PENDING;
    }

    if (hmap["Content-Length"].toInt() > 10000000) // max 10Mb uploads
    {
        return HTTPByteArray::HTTP_MEMORY_FULL;
    }

    // read body
    qDebug() << "blength " << arr.size();
    data.append(arr);

    if (hmap["Content-Length"].toInt() > data.length())
    {
        return HTTPByteArray::HTTP_PENDING;
    }
    else if (data.length() > hmap["Content-Length"].toInt())
    {
        return HTTPByteArray::HTTP_MEMORY_FULL;
    }
    else
    {
      //  headerIsReady = false;
        qDebug() << "parse body";
    }

    printDebug();
    return HTTPByteArray::HTTP_OK;
}

QString HTTPByteArray::getHeader(const QString key)
{
    if (hmap.contains(key))
    {
        return hmap[key].toString();
    }

    return "";
}

QString HTTPByteArray::getArg(const QString key)
{
    if (args.contains(key))
    {
        return args[key].toString();
    }

    throw ("Key not found");
}

void HTTPByteArray::clear()
{
    data.clear();
    hmap.clear();
    args.clear();
    header.clear();
    function.clear();
    controller.clear();
}
