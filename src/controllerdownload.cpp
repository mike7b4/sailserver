#include <QFile>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QUrl>
#include <QJsonArray>
#include <QJsonObject>
#include <QMimeDatabase>
#include <QJsonDocument>
#include "sailhttpserver.h"
#include "controllerdownload.h"

ControllerDownload::ControllerDownload(SailClient *parent) :
    ControllerBase(parent)
{
}

void ControllerDownload::handleResponse()
{
    QUrl path ("/"+controller+"/"+function+"/"+client->getHttpObject()->getPath()); // getPath splits away controller...
    QDir dir(path.toString());
    QFile file(path.toString());
    if (!dir.exists())
        downloadFile(file.fileName());
    else
        listFiles(function, path.toString());
}

void ControllerDownload::listFiles(QString title, QString path)
{
    HTTPByteArray *http = client->getHttpObject();
    QDir dir;
    // always replace pictures with Pictures notice this will break if user has a dir like Pictures/pictures/
    dir = QDir(path);
    // go back
    QStringList l = QString(controller+"/"+function+"/"+http->getPath()).split("/", QString::SkipEmptyParts);
    l.removeLast();

    appendMenu(tr("Home"), QUrl(proto+http->getHeader("Host")+"/home").toString());
    appendMenu(tr("Upload"), QUrl(proto+http->getHeader("Host")+"/upload").toString());
    appendMenu(tr("Clipboard"), QUrl(proto+http->getHeader("Host")+"/clipboard").toString());
    appendMenu(tr("Latest shot"), QUrl(proto+http->getHeader("Host")+"/latest/camera.html").toString());
    mergeMenus();

    QFileInfoList lines = dir.entryInfoList();
    append("<table>");
    append("<tr><th>"+title+"</th></tr>");
    append("<tr><td class='goback'>");
    append(tr("Up"), QUrl(proto+http->getHeader("Host")+"/"+l.join("/")).toEncoded());
    append("</td></tr>");
    foreach (QFileInfo finfo, lines)
    {
        if (finfo.fileName() != ".." && finfo.fileName() != ".")
        {
            bool isfile = !finfo.isDir();
            if (!isfile)
                append("<tr><td class='directory'>");
            else
                append("<tr><td class='file'>");

            append(finfo.fileName(), QString(QUrl(proto+http->getHeader("Host")+"/"+controller+"/"+function+"/"+http->getPath()+"/"+finfo.fileName()).toEncoded()), isfile, "<br />\n");
            append("</td></tr>");
        }
    }
    append("</table>");

    sendData(title);
}

void  ControllerDownload::downloadFile(QString file)
{
    QFile fn(file);
    qDebug() << fn.fileName();
    if (fn.exists())
    {
        QMimeDatabase db;
        QMimeType mime = db.mimeTypeForFile(file);
        if (fn.open(QIODevice::ReadOnly))
        {
            client->responseOK(false, false, mime.name());
            client->write(QString("Content-Length: %1\r\n\r\n").arg(fn.size()).toUtf8());
            client->write(fn.readAll());
            fn.close();
            return ;
        }
    }

    client->notFound();
}

