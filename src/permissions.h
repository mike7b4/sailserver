#ifndef PERMISSIONS_H
#define PERMISSIONS_H

#include <QObject>
#include <QHash>

class Path
{
    public:
        bool writeable;
        QString path;
        explicit Path(QString realname, bool write):writeable(write),path(realname){};

};
class Permissions : public QObject
{
    Q_OBJECT
    QHash<QString, Path*>allowedPaths;
    QString apikey;
public:
    explicit Permissions(QObject *parent = 0);
    ~Permissions();
    bool isAllowedPath(QString suburl);
    bool isAllowedWritePath(QString suburl);
    QString getRealPath(QString suburl);
signals:

public slots:
    void addAllowedPath(QString symlink, QString realname, bool write);

};

#endif // PERMISSIONS_H
