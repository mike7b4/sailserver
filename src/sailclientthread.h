#ifndef SAILSERVERTHREAD_H
#define SAILSERVERTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QList>
#include <QSslSocket>
#include <QStringList>
#include <QRunnable>
#include "httpbytearray.h"
class SailHttpServer;

class SailClient : public QObject
{
    Q_OBJECT
public:
    explicit SailClient(QObject *parent, SailHttpServer *serv, QString host, QSslSocket *socketDescriptor);
private:
    QString host;
    HTTPByteArray http;
    QByteArray receivedData;
    QSslSocket *pTcpSocket;
    SailHttpServer *pServer;
    QByteArray body;
public:
    void responseOK(bool endhead = true, bool chunkbody = true, QString contenttype="text/html; charset=\"utf-8\"");
    void badRequest(QString str = "<h1>Bad Request</h1>\n");
    void parsePOST(QStringList list);
    void notFound(QString whut="<h1>Not found</h1>");
    void denied(QString str="<h1>Denied</h1>");
    void beginHtml(QString title, QString metadata = "", QString extrahead = "");
    void endHtml(bool addendtag=true);
    void responseCONTINUE();
    void responseControl();
    void write(const QByteArray &data);
    void appendBody(const QByteArray &data);
    void appendBodyS(QString data){ appendBody(data.toUtf8()); };
    void appendBR(){ appendBody("<br />"); };
    void appendH1(QString C){ appendBody("<h1>"); appendBodyS(C); appendBody("</h1>");}
    void forbidden(QString str);
    void close();// { if (pTcpSocket){ pTcpSocket->close(); } }; //delete pTcpSocket; pTcpSocket = NULL;}  };
    SailHttpServer * getServer() { return pServer;};
    HTTPByteArray * getHttpObject() { return &http; };
    bool isEncrypted() { return pTcpSocket->isEncrypted(); };
    //bool isShutdown() { return (pTcpSocket ==  NULL || !pTcpSocket->isOpen());};
signals:
    void error(QTcpSocket::SocketError socketError);

public slots:
    void onError(QAbstractSocket::SocketError);
    void onSslErrors ( const QList<QSslError>& errors );
    void onReadyRead();
    void onDisconnected();
    void onEncrypted();
};

class SailClientThread : public QRunnable
{
    SailHttpServer *pServer;
    QString host;
    int socketDescriptor;
    SailClient *client;
    void run();
public:
    explicit SailClientThread(SailHttpServer *parent, QString host, int socketDescriptor);
};

#endif // SAILSERVERTHREAD_H
