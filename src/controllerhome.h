#ifndef CONTROLLERHOME_H
#define CONTROLLERHOME_H
#include <QObject>
#include "sailclientthread.h"
#include "controllerdownload.h"
class ControllerHome : public ControllerDownload
{
    Q_OBJECT
public:
    explicit ControllerHome(SailClient *parent);
    void handleResponse();
    void listFiles(QString title, QString path);
//    void downloadFile(QString file);
signals:

public slots:

};

#endif // CONTROLLERHOME_H
