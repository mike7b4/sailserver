#ifndef HTTPBYTEARRAY_H
#define HTTPBYTEARRAY_H
#include <QObject>
#include <QVariant>
#include <QStringList>
#include <QMap>
#include <QByteArray>

class HTTPByteArray : public QObject
{
    Q_OBJECT
    QMap <QString, QVariant>hmap;
    QMap <QString, QVariant>args;
    QByteArray header;
    QByteArray data;
    QString controller;
    QString function;
    bool headerIsReady;
    int contentLength; //
    int multiContentLength; // multipart 1
    void parseHeaderLines(QStringList &list);
public:
    enum HttpResponse{HTTP_INVALID_REQUEST, HTTP_OK, HTTP_DENIED, HTTP_PENDING, HTTP_REDIRECT, HTTP_MEMORY_FULL};
    explicit HTTPByteArray(QObject *parent = 0);

    void printDebug();
    HttpResponse append(QByteArray &arr);
    QString getHeader(const QString value);
    QString getArg(const QString args);
    QByteArray &getData() { return data; };
    const QString &getFunction() {    return function;};
    const QString &getController() {    return controller;};
    const QString getPath(){ return hmap["url"].toString(); };
    bool wantJSONResponse(){    return args["json"].toBool(); };
    bool hasHeader(){ return headerIsReady; }
    void clear();
signals:

public slots:

};

#endif // HTTPBYTEARRAY_H
