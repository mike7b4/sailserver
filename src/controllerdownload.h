#ifndef CONTROLLERDOWNLOAD_H
#define CONTROLLERDOWNLOAD_H

#include <QObject>
#include <QJsonArray>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QUrl>
#include <QJsonArray>
#include <QJsonObject>
#include <QMimeDatabase>
#include <QJsonDocument>
#include "sailhttpserver.h"
#include "sailclientthread.h"
#include "controllerbase.h"

class ControllerDownload : public ControllerBase
{
    Q_OBJECT
public:
    explicit ControllerDownload(SailClient *parent);
    virtual void handleResponse();
    void downloadFile(QString file);
    virtual void listFiles(QString title, QString path);
signals:

public slots:

};

#endif // CONTROLLERDOWNLOAD_H
