#ifndef CONTROLLERUPLOAD_H
#define CONTROLLERUPLOAD_H

#include <QObject>
#include "sailclientthread.h"
#include "httpbytearray.h"
#include "sailhttpserver.h"
#include "controllerbase.h"
class ControllerUpload : public ControllerBase
{
    Q_OBJECT
public:
    explicit ControllerUpload(SailClient *parent = 0);

    void parse();
    void handleResponse();
    QString concatFileName(QString &fname);
signals:

public slots:

};

#endif // CONTROLLERUPLOAD_H
