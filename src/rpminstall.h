#ifndef DBUSMONITOR_H
#define DBUSMONITOR_H
#include <QtDBus/QDBusInterface>
#include <QObject>

class RpmInstall : public QObject
{
    Q_OBJECT
    QString installFilename;
   // QDBusInterface ifaceStore;
public:
    explicit RpmInstall(QObject *parent = 0);
    void run(QString installpackage);
signals:

public slots:
    void onPackageStatusChanged(const QString &filename, int status);
};

#endif // DBUSMONITOR_H
