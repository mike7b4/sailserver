#ifndef CONTROLLERBASE_H
#define CONTROLLERBASE_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QUrl>
#include <QJsonArray>
#include <QList>
#include <QHash>
#include <QString>
#include <QJsonObject>
#include <QMap>
#include <QMimeDatabase>
#include <QJsonDocument>
#include "sailhttpserver.h"
#include "sailclientthread.h"

class ControllerBase : public QObject
{
    Q_OBJECT
protected:
    SailClient *client;
    QString proto; // http/https
    typedef QMap<QString, QString> myMapType;
    QList<myMapType>menus;
    QJsonArray array; // json returned
    QByteArray data;
    QString extracontent; // not sent if .json request
    QString function;
    QString controller;
    /* oh this starting lock like redo */
    void append(QString title, QString url = "", bool isFile=false, QString lineend="");
    void appendMenu(QString title, QString url);
    void mergeMenus();
    void sendData(QString title, bool end = true);
    void parsePost();
public:
    explicit ControllerBase(SailClient *parent);

signals:

public slots:

};

#endif // CONTROLLERBASE_H
