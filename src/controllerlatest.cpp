#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QString>
#include "controllerlatest.h"

ControllerLatest::ControllerLatest(SailClient *parent) :
    ControllerBase(parent)
{
}

void ControllerLatest::handleResponse()
{
    HTTPByteArray *http = client->getHttpObject();
    if (client->getHttpObject()->wantJSONResponse())
    {
        append("latest_shot", proto+http->getHeader("Host")+"/home/Pictures/Camera/"+client->getServer()->getLatestPicture());
        sendData("Latest shot");
    }
    else
    {
        client->responseOK();
        int refresh = client->getServer()->getCameraRefresh()*1000;
        QFile file(client->getServer()->getAppInfo()->getConfigPath()+"/latest.html");

        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line = in.readLine();

                line = line.replace("<% refresh_image %>", QString("%1").arg(refresh));
                line = line.replace("<% site_ip %>", proto+http->getHeader("Host"));
                client->appendBodyS(line+"\n");
            }
        }
        client->endHtml(false); // false == no endtag since we use file and it already have </body></html>
    }
}
