<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="7"/>
        <source>About</source>
        <translation type="unfinished">Om</translation>
    </message>
</context>
<context>
    <name>ChangeLog</name>
    <message>
        <location filename="../qml/pages/ChangeLog.qml" line="7"/>
        <source>ChangeLog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ControllerBase</name>
    <message>
        <location filename="../src/controllerbase.cpp" line="73"/>
        <source>Sailserver - %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ControllerClipboard</name>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="27"/>
        <source>Sailserver - Clipboard updated</source>
        <translation type="unfinished">Sailserver - Klippbord updaterad</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="28"/>
        <source>Clipboard updated :)</source>
        <translation type="unfinished">Klippbord updaterad</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="31"/>
        <source>Clipboard updated</source>
        <translation>Klippbord uppdaterad</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="40"/>
        <source>Home</source>
        <translation type="unfinished">Hemkatalog</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="42"/>
        <source>Clipboard</source>
        <translation type="unfinished">Klippbord</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="41"/>
        <source>Upload</source>
        <translation type="unfinished">Ladda upp</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="45"/>
        <source>Set clipboard: </source>
        <translation type="unfinished">Sätt klippbord</translation>
    </message>
</context>
<context>
    <name>ControllerDownload</name>
    <message>
        <location filename="../src/controllerdownload.cpp" line="39"/>
        <source>Home</source>
        <translation type="unfinished">Hemkatalog</translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="40"/>
        <source>Upload</source>
        <translation type="unfinished">Ladda upp</translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="41"/>
        <source>Clipboard</source>
        <translation type="unfinished">Klippbord</translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="42"/>
        <source>Latest shot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="49"/>
        <source>Up</source>
        <translation type="unfinished">Bakåt</translation>
    </message>
</context>
<context>
    <name>ControllerHome</name>
    <message>
        <location filename="../src/controllerhome.cpp" line="68"/>
        <source>Home</source>
        <translation type="unfinished">Hemkatalog</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="70"/>
        <source>Clipboard</source>
        <translation type="unfinished">Klippbord</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="69"/>
        <source>Upload</source>
        <translation type="unfinished">Ladda upp</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="71"/>
        <source>Latest shot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="78"/>
        <source>Documents</source>
        <translation type="unfinished">Dokument</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="82"/>
        <source>Music</source>
        <translation type="unfinished">Musik</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="86"/>
        <source>Pictures</source>
        <translation type="unfinished">Bilder</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="90"/>
        <source>Videos</source>
        <translation type="unfinished">Film</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="94"/>
        <source>Downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="98"/>
        <source>SDCard</source>
        <translation type="unfinished">SD kort</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="104"/>
        <source>Set clipboard: </source>
        <translation type="unfinished">Sätt klippbord</translation>
    </message>
</context>
<context>
    <name>ControllerUpload</name>
    <message>
        <location filename="../src/controllerupload.cpp" line="65"/>
        <source>File already exists :(</source>
        <translation type="unfinished">Filen finns redan på din Jolla :(</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="66"/>
        <source>Upload failed :(</source>
        <translation type="unfinished">Uppladdning misslyckades</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="67"/>
        <source>&apos;%1&apos; already exists!</source>
        <translation type="unfinished">&apos;%1&apos; existerar redan!</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="68"/>
        <location filename="../src/controllerupload.cpp" line="81"/>
        <location filename="../src/controllerupload.cpp" line="96"/>
        <location filename="../src/controllerupload.cpp" line="111"/>
        <source>Home</source>
        <translation type="unfinished">Hemkatalog</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="71"/>
        <source>File &apos;%1&apos; already exists!</source>
        <translation type="unfinished">Filen &apos;%1&apos; finns redan!</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="78"/>
        <location filename="../src/controllerupload.cpp" line="79"/>
        <source>Upload succeed :)</source>
        <translation type="unfinished">Uppladdningen lyckades :)</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="84"/>
        <source>%1 uploaded :)</source>
        <translation type="unfinished">%1 uppladdad</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="94"/>
        <source>Upload failed! Could not create &apos;%1&apos; :(</source>
        <translation type="unfinished">Uppladdning misslyckades. Kunde inte skapa %1</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="93"/>
        <source>Could not create file :(</source>
        <translation type="unfinished">Filen kunde inte skapas :(</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="99"/>
        <source>Failed to create
&apos;%1&apos; :/</source>
        <translation type="unfinished">Kunde inte skapa %1 :/</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="110"/>
        <location filename="../src/controllerupload.cpp" line="112"/>
        <location filename="../src/controllerupload.cpp" line="116"/>
        <location filename="../src/controllerupload.cpp" line="117"/>
        <source>Upload</source>
        <translation type="unfinished">Ladda upp</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="113"/>
        <source>Clipboard</source>
        <translation type="unfinished">Klippbord</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="118"/>
        <source>(Max filesize is 10Mb)</source>
        <translation type="unfinished">(Max filstorlek är 10Mb)</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="123"/>
        <source>If you want to secure the server you can upload a server.key and server.crt and it will be automatic stored in .config/harbour-sailserver/ directory and after that you can use https://%1 instead of http://%2</source>
        <translation type="unfinished">Om du vill säkra servern kan du ladda upp server.key och server.crt. Den kommer då automatiskt flyttas till .config/harbour-sailserver/ och därefter kan du accessa din jolla via https://%2 istället för http://%1</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="66"/>
        <source>Running</source>
        <translation type="unfinished">Aktiv</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="66"/>
        <source>Stopped</source>
        <translation type="unfinished">Inaktiv</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="45"/>
        <source>About</source>
        <translation type="unfinished">Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="49"/>
        <source>Settings</source>
        <translation type="unfinished">Inställningar</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="77"/>
        <source>Server running</source>
        <translation type="unfinished">Server aktiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="77"/>
        <source>Server stopped</source>
        <translation type="unfinished">Server inaktiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="106"/>
        <source>Requests: %1</source>
        <translation type="unfinished">Antal webclient förfrågningar: %1</translation>
    </message>
</context>
<context>
    <name>LicensePage</name>
    <message>
        <location filename="../qml/pages/LicensePage.qml" line="7"/>
        <source>License</source>
        <translation type="unfinished">Licens</translation>
    </message>
</context>
<context>
    <name>SailClient</name>
    <message>
        <location filename="../src/sailclientthread.cpp" line="174"/>
        <source>The file was too big :(</source>
        <translation type="unfinished">Filen är för stor</translation>
    </message>
    <message>
        <location filename="../src/sailclientthread.cpp" line="177"/>
        <source>File was too big aborted</source>
        <translation type="unfinished">Filen är för stor</translation>
    </message>
    <message>
        <location filename="../src/sailclientthread.cpp" line="181"/>
        <source>Sailserver - Redirect to home</source>
        <translation type="unfinished">Sailserver redirekt till hemkatalog</translation>
    </message>
    <message>
        <location filename="../src/sailclientthread.cpp" line="182"/>
        <source>Redirect to home in a 1 second</source>
        <translation type="unfinished">Sailserver redirekt till hemkatalog om 1 sekund</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="35"/>
        <source>Activate server when app start</source>
        <translation type="unfinished">Aktivera server när app startas</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="26"/>
        <source>Enter port &gt; 1024</source>
        <translation type="unfinished">Skriv in port du vill använda</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="27"/>
        <source>Server Port</source>
        <translation type="unfinished">Server port</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="44"/>
        <source>Allow install RPM&apos;s
(Notice! OS will ask first!)</source>
        <translation type="unfinished">Tillåt installation av paket
(Notera att SailfishOS frågar först!)</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="53"/>
        <source>Remove package after install</source>
        <translation type="unfinished">Radera paket efter installation</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="74"/>
        <source>requests latest photo every(seconds):</source>
        <translation>Begär foto varje (sekunder):</translation>
    </message>
</context>
</TS>
