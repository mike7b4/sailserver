<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="7"/>
        <source>About</source>
        <translation>About</translation>
    </message>
</context>
<context>
    <name>ChangeLog</name>
    <message>
        <location filename="../qml/pages/ChangeLog.qml" line="7"/>
        <source>ChangeLog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ControllerBase</name>
    <message>
        <location filename="../src/controllerbase.cpp" line="73"/>
        <source>Sailserver - %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ControllerClipboard</name>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="27"/>
        <source>Sailserver - Clipboard updated</source>
        <translation>Leikepöydälle päivitetty</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="28"/>
        <source>Clipboard updated :)</source>
        <translation>Leikepöydälle päivitetty</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="31"/>
        <source>Clipboard updated</source>
        <translation>Leikepöydälle päivitetty</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="40"/>
        <source>Home</source>
        <translation>Koti</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="42"/>
        <source>Clipboard</source>
        <translation>Leikepöydälle</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="41"/>
        <source>Upload</source>
        <translation>Lähetä</translation>
    </message>
    <message>
        <location filename="../src/controllerclipboard.cpp" line="45"/>
        <source>Set clipboard: </source>
        <translation>Aseta leikepöydälle</translation>
    </message>
</context>
<context>
    <name>ControllerDownload</name>
    <message>
        <location filename="../src/controllerdownload.cpp" line="39"/>
        <source>Home</source>
        <translation>Koti</translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="40"/>
        <source>Upload</source>
        <translation>Lähetä</translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="41"/>
        <source>Clipboard</source>
        <translation>Leikepöydälle</translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="42"/>
        <source>Latest shot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controllerdownload.cpp" line="49"/>
        <source>Up</source>
        <translation>taaksepäin</translation>
    </message>
</context>
<context>
    <name>ControllerHome</name>
    <message>
        <location filename="../src/controllerhome.cpp" line="68"/>
        <source>Home</source>
        <translation>Koti</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="70"/>
        <source>Clipboard</source>
        <translation>Leikepöydälle</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="69"/>
        <source>Upload</source>
        <translation>Lähetä</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="71"/>
        <source>Latest shot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="78"/>
        <source>Documents</source>
        <translation>Dokumentti</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="82"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="86"/>
        <source>Pictures</source>
        <translation>Kuvat</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="90"/>
        <source>Videos</source>
        <translation>Film</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="94"/>
        <source>Downloads</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="98"/>
        <source>SDCard</source>
        <translation>SD-kortti</translation>
    </message>
    <message>
        <location filename="../src/controllerhome.cpp" line="104"/>
        <source>Set clipboard: </source>
        <translation>Aseta leikepöydälle</translation>
    </message>
</context>
<context>
    <name>ControllerUpload</name>
    <message>
        <location filename="../src/controllerupload.cpp" line="65"/>
        <source>File already exists :(</source>
        <translation>jo olemassa</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="66"/>
        <source>Upload failed :(</source>
        <translation>Lataus epäonnistui</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="67"/>
        <source>&apos;%1&apos; already exists!</source>
        <translation>&apos;%1&apos; tiedosto on!</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="68"/>
        <location filename="../src/controllerupload.cpp" line="81"/>
        <location filename="../src/controllerupload.cpp" line="96"/>
        <location filename="../src/controllerupload.cpp" line="111"/>
        <source>Home</source>
        <translation>Koti</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="71"/>
        <source>File &apos;%1&apos; already exists!</source>
        <translation>%1 jo olemassa!</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="78"/>
        <location filename="../src/controllerupload.cpp" line="79"/>
        <source>Upload succeed :)</source>
        <translation>Lähetä onnistua</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="84"/>
        <source>%1 uploaded :)</source>
        <translation>%1 Lisätty</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="94"/>
        <source>Upload failed! Could not create &apos;%1&apos; :(</source>
        <translation>Lataus epäonnistui %1</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="93"/>
        <source>Could not create file :(</source>
        <translation>Ei voitu luoda tiedostoa</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="99"/>
        <source>Failed to create
&apos;%1&apos; :/</source>
        <translation>Ei voinut luoda %1</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="110"/>
        <location filename="../src/controllerupload.cpp" line="112"/>
        <location filename="../src/controllerupload.cpp" line="116"/>
        <location filename="../src/controllerupload.cpp" line="117"/>
        <source>Upload</source>
        <translation>Lähetä</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="113"/>
        <source>Clipboard</source>
        <translation>Kirjoituslevy</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="118"/>
        <source>(Max filesize is 10Mb)</source>
        <translation>(Max tiedostokoko on 10MB)</translation>
    </message>
    <message>
        <location filename="../src/controllerupload.cpp" line="123"/>
        <source>If you want to secure the server you can upload a server.key and server.crt and it will be automatic stored in .config/harbour-sailserver/ directory and after that you can use https://%1 instead of http://%2</source>
        <translation>Jos haluat varmistaa palvelimelle voit ladata server.key ja server.crt ja se on automaattinen tallennetaan .config/harbour-sailserver/ hakemistoon ja sen jälkeen voit käyttää https://%1 ei http://%2</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="66"/>
        <source>Running</source>
        <translation>Aktiivinen</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="66"/>
        <source>Stopped</source>
        <translation>Toimeton</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="45"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="49"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="77"/>
        <source>Server running</source>
        <translation>Server aktiivinen</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="77"/>
        <source>Server stopped</source>
        <translation>Server toimeton</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="106"/>
        <source>Requests: %1</source>
        <translation>Webclient pyynnöt %1</translation>
    </message>
</context>
<context>
    <name>LicensePage</name>
    <message>
        <location filename="../qml/pages/LicensePage.qml" line="7"/>
        <source>License</source>
        <translation>Lisenssi</translation>
    </message>
</context>
<context>
    <name>SailClient</name>
    <message>
        <location filename="../src/sailclientthread.cpp" line="174"/>
        <source>The file was too big :(</source>
        <translation>tiedosto on liian suuri</translation>
    </message>
    <message>
        <location filename="../src/sailclientthread.cpp" line="177"/>
        <source>File was too big aborted</source>
        <translation>tiedosto on liian suuri</translation>
    </message>
    <message>
        <location filename="../src/sailclientthread.cpp" line="181"/>
        <source>Sailserver - Redirect to home</source>
        <translation>Uudelleenohjautua kotiin</translation>
    </message>
    <message>
        <location filename="../src/sailclientthread.cpp" line="182"/>
        <source>Redirect to home in a 1 second</source>
        <translation>Uudelleenohjautua kotiin</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="35"/>
        <source>Activate server when app start</source>
        <translation type="unfinished">Aktivoi palvelimelle app alku</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="26"/>
        <source>Enter port &gt; 1024</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="27"/>
        <source>Server Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="44"/>
        <source>Allow install RPM&apos;s
(Notice! OS will ask first!)</source>
        <translation>Salli asennuspakkauksia
(Huom! OS kysyy ensin!)</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="53"/>
        <source>Remove package after install</source>
        <translation>Poista paketti asennuksen jälkeen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="74"/>
        <source>requests latest photo every(seconds):</source>
        <translation>pyytää uusin valokuva välein: (sekuntia)</translation>
    </message>
</context>
</TS>
